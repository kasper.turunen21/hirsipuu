import random

# Pelin aloitus
print("Tervetuloa pelamamaan hirsipuuta")
nimi = input("Nimesi: ")
print("Moro " + nimi + "! Onnea!")

print("Peli alkaa!")



# Pelin toiminta
def main():
    global määrä
    global näyttö
    global sana
    global arvattu
    global pituus
    global pelaa
    sanat = ["kirahvi", "miekkavalas", "leijona"]
    sana = random.choice(sanat)
    pituus = len(sana)
    määrä = 0
    näyttö = '_' * pituus
    arvattu = []
    pelaa = ""

# Looppi pelata uudestaa

def play_loop():
    global pelaa
    pelaa = input("Pelaatko uudestaan? k = yes, e = no ")
    while pelaa not in ["k", "e","K","E"]:
        pelaa = input("Pelaatko uudestaan? k = yes, e = no ")
    if pelaa == "k":
        main()
    elif pelaa == "n":
        print("Kiitos pelaamisesta")
        exit()

# Pelin käynnistys
def hirsipuu():
    global määrä
    global näyttö
    global sana
    global arvattu
    global pelaa
    elämät = 5
    arvaus = input("Tämä on sana: " + näyttö + " Arvaus: ")
    arvaus = arvaus.strip()
    if len(arvaus.strip()) == 0 or len(arvaus.strip()) >= 2 or arvaus <= "9":
        print("Väärä syöttö, yritä uudestaan")
        hirsipuu()


    elif arvaus in sana:
        arvattu.extend([arvaus])
        indeksi = sana.find(arvaus)
        sana = sana[:indeksi] + "_" + sana[indeksi + 1:]
        näyttö = näyttö[:indeksi] + arvaus + näyttö[indeksi + 1:]
        print(näyttö + "\n")

    elif arvaus in arvattu:
        print("Yritä uudestaan.\n")

    else:
        määrä += 1

        if määrä == 1:
    
            print("   _____ \n"
                  "  |      \n"
                  "  |      \n"
                  "  |      \n"
                  "  |      \n"
                  "  |      \n"
                  "  |      \n"
                  "__|__\n")
            print("Väärä arvaus. " + str(elämät - määrä) + " arvausta jäljellä\n")

        elif määrä == 2:
            
            print("   _____ \n"
                  "  |     | \n"
                  "  |     |\n"
                  "  |      \n"
                  "  |      \n"
                  "  |      \n"
                  "  |      \n"
                  "__|__\n")
            print("Väärä arvaus. " + str(elämät - määrä) + " arvausta jäljellä\n")

        elif määrä == 3:
           
           print("   _____ \n"
                 "  |     | \n"
                 "  |     |\n"
                 "  |     | \n"
                 "  |      \n"
                 "  |      \n"
                 "  |      \n"
                 "__|__\n")
           print("Väärä arvaus. " + str(elämät - määrä) + " arvausta jäljellä\n")

        elif määrä == 4:
            
            print("   _____ \n"
                  "  |     | \n"
                  "  |     |\n"
                  "  |     | \n"
                  "  |     O \n"
                  "  |      \n"
                  "  |      \n"
                  "__|__\n")
            print("Väärä arvaus. " + str(elämät - määrä) + " viimeinen arvaus\n")

        elif määrä == 5:
        
            print("   _____ \n"
                  "  |     | \n"
                  "  |     |\n"
                  "  |     | \n"
                  "  |     O \n"
                  "  |    /|\ \n"
                  "  |    / \ \n"
                  "__|__\n")
            print("Väärä arvaus. Sinut on hirtetty!!!\n")
            print("The word was:",arvattu,sana)
            play_loop()

    if sana == '_' * pituus:
        print("Onnitelut arvasit sanan!")
        play_loop()

    elif määrä != elämät:
        hirsipuu()


main()


hirsipuu()

